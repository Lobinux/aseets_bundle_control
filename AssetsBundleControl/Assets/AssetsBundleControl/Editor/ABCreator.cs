﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEditor;
using System.IO;

public class ABCreator{

	[MenuItem("Assets/Build Asset Bundle For Windows")]
    static void BuildBundlesForWindows()
    {
        if (!Directory.Exists(Application.dataPath + "/AssetBundle/Windows/32"))
        {
            Directory.CreateDirectory(Application.dataPath + "/AssetBundle/Windows/32");
        }

        if (!Directory.Exists(Application.dataPath + "/AssetBundle/Windows/64"))
        {
            Directory.CreateDirectory(Application.dataPath + "/AssetBundle/Windows/64");
        }
        BuildPipeline.BuildAssetBundles("Assets/AssetBundle/Windows/32", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows);
        BuildPipeline.BuildAssetBundles("Assets/AssetBundle/Windows/64", BuildAssetBundleOptions.None, BuildTarget.StandaloneWindows64);
    }

    [MenuItem("Assets/Build Asset Bundle For MAC")]
    static void BuildBundlesForMac()
    {
        if (!Directory.Exists(Application.dataPath + "/AssetBundle/MAC"))
        {
            Directory.CreateDirectory(Application.dataPath + "/AssetBundle/MAC");
        }

        BuildPipeline.BuildAssetBundles("Assets/AssetBundle/MAC", BuildAssetBundleOptions.None, BuildTarget.StandaloneOSXUniversal);
    }

    [MenuItem("Assets/Build Asset Bundle For Linux")]
    static void BuildBundlesForLinux()
    {
        if (!Directory.Exists(Application.dataPath + "/AssetBundle/Linux"))
        {
            Directory.CreateDirectory(Application.dataPath + "/AssetBundle/Linux");
        }

        BuildPipeline.BuildAssetBundles("Assets/AssetBundle/Linux", BuildAssetBundleOptions.None, BuildTarget.StandaloneLinuxUniversal);
    }

    [MenuItem("Assets/Build Asset Bundle For WebGL")]
    static void BuildBundlesForWebGL()
    {
        if (!Directory.Exists(Application.dataPath + "/AssetBundle/WebGL"))
        {
            Directory.CreateDirectory(Application.dataPath + "/AssetBundle/WebGL");
        }

        BuildPipeline.BuildAssetBundles("Assets/AssetBundle/WebGL", BuildAssetBundleOptions.None, BuildTarget.WebGL);
    }

    [MenuItem("Assets/Build Asset Bundle For Android")]
    static void BuildBundlesForAndroid()
    {
        if (!Directory.Exists(Application.dataPath + "/AssetBundle/Android"))
        {
            Directory.CreateDirectory(Application.dataPath + "/AssetBundle/Android");
        }

        BuildPipeline.BuildAssetBundles("Assets/AssetBundle/Android", BuildAssetBundleOptions.None, BuildTarget.Android);
    }

    [MenuItem("Assets/Build Asset Bundle For IOS")]
    static void BuildBundlesForIOS()
    {
        if (!Directory.Exists(Application.dataPath + "/AssetBundle/IOS"))
        {
            Directory.CreateDirectory(Application.dataPath + "/AssetBundle/IOS");
        }

        BuildPipeline.BuildAssetBundles("Assets/AssetBundle/Android", BuildAssetBundleOptions.None, BuildTarget.iOS);
    }
}
#endif
