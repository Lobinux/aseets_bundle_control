﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

public class AssetBundleContainer : MonoBehaviour {

    [SerializeField]
    List<string> urls = new List<string>();

    CanvasGroup cg;

    [SerializeField]
    List<AssetBundle> assetBundles = new List<AssetBundle>();

    [SerializeField]
    List<string> bundles = new List<string>();

    float t = 0;

    bool loadComplete = false;

    static AssetBundleContainer assetBundleContainer;

    public static AssetBundleContainer GetInstance()
    {
        if (!assetBundleContainer)
        {
            Debug.LogError("The Asset Bundle Container prefab isn't in the scene");
        }
        return assetBundleContainer;
    }

    private void Awake()
    {
        if (!assetBundleContainer)
        {
            assetBundleContainer = this.GetComponent<AssetBundleContainer>();
        }
        assetBundleContainer.cg = this.GetComponent<CanvasGroup>();

        DontDestroyOnLoad(this);
    }

    private void Start()
    {
        StartCoroutine(DownloadAndCache());
    }

    // Use this for initialization
    IEnumerator DownloadAndCache()
    {
        if (assetBundles.Count == 0)
        {
            for (int i = 0; i < urls.Count; i++)
            {
                using (WWW www = WWW.LoadFromCacheOrDownload(urls[i],0))
                {
                    yield return www;
                    if (!string.IsNullOrEmpty(www.error))
                    {
                        Debug.LogError(www.error);
                        yield break;
                    }

                    assetBundles.Add(www.assetBundle);
                    bundles.AddRange(www.assetBundle.GetAllAssetNames());
                }
            }
        }

        foreach (var bundle in bundles)
        {
            Debug.Log(Path.GetFileNameWithoutExtension(bundle));
        }
        StartCoroutine(OnLoadingEnded());
    }

    IEnumerator OnLoadingEnded()
    {
        
        while (t <= 1)
        {
            t += Time.deltaTime * .5f;
            cg.alpha = Mathf.Lerp(1f, 0f, t);
        }

        loadComplete = true;
        yield return new WaitForSecondsRealtime(1f);
    }

    public bool IsLoadComplete()
    {
        return loadComplete;
    }

    public AssetBundle GetBundleByName(string name)
    {
        AssetBundle temp = null;
        foreach (var item in assetBundles)
        {
            if (item.name == name)
            {
                temp = item;
            }
        }
        return temp;
    }
}
