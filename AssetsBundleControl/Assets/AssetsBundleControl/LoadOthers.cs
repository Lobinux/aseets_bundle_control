﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadOthers : MonoBehaviour {

    [SerializeField]
    string bundleName = "bundletest";
    [SerializeField]
    string assetToLoad = "cube";
    [SerializeField]
    GameObject temp;
    [SerializeField]
    AssetBundleContainer abc;

    private void Start()
    {
        abc = AssetBundleContainer.GetInstance();
    }

    private void Update()
    {
        if (!temp)
        {
            if (abc)
            {
                if (abc.IsLoadComplete())
                {
                    temp = Instantiate(abc.GetBundleByName(bundleName).LoadAsset<GameObject>(assetToLoad));
                }
            }
        }
    }
}
